const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('./User');

const register = async (req, res, next) => {
    const { email, password } = req.body;
    console.log('email:', email);
    console.log('password:', password);
    if(!email || !password) {
        res.status(400);
        // throw new Error('Email/password is required');
        next(new Error('Email/Password is required'));
        return;
    }
    let user = {email,password};
    user.password = bcrypt.hashSync(password, 10);
    await User.create(user);
    res.json({ message: 'User registered successfully', status: 'success' })
}

const login = async (req, res, next) => {
    const { email, password } = req.body;
    if(!email || !password) {
        res.status(400);
        // throw new Error('Email/password is required');
        next(new Error('Email/Password is required'));
        return;
    }
    const user = await User.findOne({email});
    if(!user) {
        res.status(400);
        next(new Error('Invalid credentials'));
        return;
    }
    const passwordMatch = bcrypt.compareSync(password, user.password);
    if(!passwordMatch) {
        res.status(400);
        next(new Error('Invalid credentails'));
        return;
    }
    const token = jwt.sign({ email }, process.env.JWT_SECRET);
    res.json({status: 'success', token});
}

module.exports = {
    register,
    login
}