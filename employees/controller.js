const Employee = require('./Employee');

const getAllEmployees = async (req, res) => {
    if(JSON.stringify(req.query) !== '{}') {
        if(req.query.sort === 'age') {
            const sortedEmployees = await Employee.find({}).sort({
                age: 1
            })
            res.send(sortedEmployees);
        } else {
            const sortedEmployees = await Employee.find({}).sort({
                age: -1
            })
            res.send(sortedEmployees)
        }       
    } else {
        const allEmployees = await Employee.find({})
        res.send(allEmployees);
    }  
}

const getEmployee = async (req, res) => {    
    const {id} = req.params;
    const employee = await Employee.findById(id);
    res.send(employee);
}

const createEmployee = async(req, res) => {
    const data = req.body;
    const newEmployee = new Employee(data);
    const response = await newEmployee.save();
    res.send(response);
}

const updateEmployee = async (req, res) => {
    const {id} = req.params;
    const data = req.body;
    const updatedEmployee = await Employee.findByIdAndUpdate(id, data);
    res.send(updatedEmployee);
}

const deleteEmployee = async (req, res) => {
    const {id} = req.params;
    const deletedEmployee = await Employee.findByIdAndDelete(id)
    res.json(deletedEmployee);
}

const sortEmployeeByAge = async (req, res) => {
    console.log('req query', req);
}

module.exports = {
    getAllEmployees,
    getEmployee,
    createEmployee,
    updateEmployee,
    deleteEmployee,
    sortEmployeeByAge
}