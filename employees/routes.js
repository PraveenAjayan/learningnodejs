const express = require('express');
const router = express.Router();
const employeeController = require('./controller');
const authCheck = require('../auth/authCheck');

router.get('/', authCheck, employeeController.getAllEmployees);
router.get('/:id', authCheck, employeeController.getEmployee);
router.post('/', authCheck, employeeController.createEmployee);
router.put('/:id', authCheck, employeeController.updateEmployee);
router.delete('/:id', authCheck, employeeController.deleteEmployee);
router.get('/', authCheck, employeeController.sortEmployeeByAge);

module.exports = router;