const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const employeesRouter = require('./employees/routes');
const authRouter = require('./auth/routes');


dotenv.config();

const app = express();
app.use(express.json());

app.get('/', (req, res)=> {
    console.log('hello from server');
    res.send('hello');
});
app.get('/error', (req, res) => {
    throw new Error('Error occured from /error');
});

app.get('/errorasync',(err, req, res, next) => {
    next(new Error('Error occured from /error'));
})

app.use('/api/employees', employeesRouter);
app.use('/auth', authRouter);

app.use((err, req, res, next) => {
    res.send({message: err.message, status: 'failure'})
});

const start = async() => {
    await mongoose.connect(process.env.MONGO_URL);
    app.listen(4000,() => {
        console.log('Server running...')
    }); 
}
start();
             